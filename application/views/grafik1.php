<html>
  <head>
<!--CSS OPEN-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>


/* Add a gray background color with some padding */
body {
  font-family: Arial;
  background: ;
}

}
</style>
<!--css close-->

    <!--Load the AJAX API-->
      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      <script type="text/javascript">
        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function drawChart() {
        	//load data from ci controller
//CHART1
        	var PieChartData='<?php echo $PieChartData;?>'; 
          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Topping');
          data.addColumn('number', 'Jumlah Wisatawan');
          data.addRows(JSON.parse(PieChartData));

          // Set chart options
          var options = {'title':'<?php echo $PieChartTitle ?>',
                         'width':1124,
                         'height':180};
          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
          chart.draw(data, options);

//CHART2
          var PieChartData2='<?php echo $PieChartData2;?>'; 
          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string','jenishotel');
          data.addColumn('number','jumlah');
          data.addRows(JSON.parse(PieChartData2));

          // Set chart options
          var options2 = {'title':'<?php echo $PieChartTitle2 ?>',
                         'width':220,
                         'height':200,
                       };
          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.PieChart(document.getElementById('chart_div2'));
          chart.draw(data, options2);

//chart3
          var PieChartData3='<?php echo $PieChartData3;?>'; 
          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string','jenwisatawan');
          data.addColumn('number','jumlah');
          data.addRows(JSON.parse(PieChartData3));

          // Set chart options
          var options3 = {'title':'<?php echo $PieChartTitle3 ?>',
                         'width':220,
                         'height':200,
                          PieHole:0.4};

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.PieChart(document.getElementById('chart_div3'));
          chart.draw(data, options3);
//chart4
          var PieChartData4='<?php echo $PieChartData4;?>'; 
          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string','jenwisatawan');
          data.addColumn('number','jumlah');
          data.addRows(JSON.parse(PieChartData4));

          // Set chart options
          var options4 = {'title':'<?php echo $PieChartTitle4 ?>',
                         'width':220,
                         'height':200,
                          PieHole:0.4};

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.PieChart(document.getElementById('chart_div4'));
          chart.draw(data, options4);

//chart5
          var PieChartData5='<?php echo $PieChartData5;?>'; 
          // Create the data table.
          var data = new google.visualization.DataTable();
          data.addColumn('string','negara');
          data.addColumn('number','jumlah');
          data.addRows(JSON.parse(PieChartData5));

          // Set chart options
          var options5 = {'title':'<?php echo $PieChartTitle5 ?>',
                         'width':1024,
                         'height':370
                          };

          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.GeoChart(document.getElementById('chart_div5'));
          chart.draw(data, options5);
        } 
      </script>
  </head>

  <body>
    <center>
    <table>
    <center><h1>Data Statistik Kepariwisataan Yogyakarta 2016</h1>
      </center>

    <!--Div that will hold the pie chart-->
    <tr>
        <td colspan="5">
      <div id="chart_div" align='center'></div>
        </td>
    </tr>
    <tr>
      <td>
        <div id="chart_div2"></div>
      </td>
      <td>
        <div id="chart_div3"></div>
      </td>
      <td>
        <div id="chart_div4"></div>
      </td>
    </tr>
    <tr>
      <td colspan="5">
      <div id="chart_div5" align='center'></div>
      </td>
    </tr>
     
        </table>
        </center>
  </body>
</html>