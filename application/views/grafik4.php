<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {'title':'<?php echo $donuttitle ?>',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="donutchart" style="width: 900px; height: 500px;"></div>
  </body>
</html>


tes

function grafik2()
  {
    $chartData=file_get_contents('assets/wisnushotel.json');
    $chartData=json_decode($chartData);
    $res=array();
    foreach($chartData as $row)
    {
      $dat=[$row->bulan,(double)$row->val];
      array_push($res, $dat);
    }
    //echo json_encode($res);
    $data['PieChartTitle']='Jumlah Wisatawan Nusantara yang menginap berdasarkan jenis hotel';
    $data['PieChartData']=json_encode($res);
    $this->load->view('grafik2',$data);
  }

function grafik4()
  {
    $chartData=file_get_contents('assets/jumlahwisatawan.json');
    $chartData=json_decode($chartData);
    $res=array();
    foreach($chartData as $row)
    {
      $dat=[$row->bulan,(double)$row->val];
      array_push($res, $dat);
    }
    //echo json_encode($res);
    $data['donuttitle']='Jumlah Wisatawan Nusantara yang menginap berdasarkan jenis hotel';
    $data['donutchartdata']=json_encode($res);
    $this->load->view('grafik4',$data);
  }
}





/// grafik1
/CHART2
          var donut_data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var donut_options = {
          'width':350,
          'height':180
        };

        var donut_chart = new google.visualization.PieChart(document.getElementById('donutchart1'));
        donut_chart.draw(donut_data, donut_options);
//CHART3
          var donut_data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var donut_options = {
          'width':350,
          'height':180
        };

        var donut_chart = new google.visualization.PieChart(document.getElementById('donutchart2'));
        donut_chart.draw(donut_data, donut_options);
//CHART2
          var donut_data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var donut_options = {
          'width':350,
          'height':180
        };

        var donut_chart = new google.visualization.PieChart(document.getElementById('donutchart3'));
        donut_chart.draw(donut_data, donut_options);
//Geo chart
        var geo_data = google.visualization.arrayToDataTable([
          ['Country', 'Popularity'],
          ['Germany', 200],
          ['United States', 300],
          ['Brazil', 400],
          ['Canada', 500],
          ['France', 600],
          ['RU', 700]
        ]);

        var geo_options = {
          'width':520,
          'height':250
        };

        var geo_chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        geo_chart.draw(geo_data, geo_options);
         }





         <td>
      <div id="donutchart1"></div>
        </td>
        <td>
      <div id="donutchart2"></div>
        </td>
        <td>
      <div id="donutchart3"></div>
        </td>
        <tr>
          <td></td>
      <td>
      <div id="regions_div" align='center'></div>
      </td>
      </tr>
      </tr>


          var donut_data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var donut_options = {
          'width':350,
          'height':180
        };

        var donut_chart = new google.visualization.PieChart(document.getElementById('donutchart2'));
        donut_chart.draw(donut_data, donut_options);
//CHART2
          var donut_data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var donut_options = {
          'width':350,
          'height':180
        };

        var donut_chart = new google.visualization.PieChart(document.getElementById('donutchart3'));
        donut_chart.draw(donut_data, donut_options);
//Geo chart
        var geo_data = google.visualization.arrayToDataTable([
          ['Country', 'Popularity'],
          ['Germany', 200],
          ['United States', 300],
          ['Brazil', 400],
          ['Canada', 500],
          ['France', 600],
          ['RU', 700]
        ]);

        var geo_options = {
          'width':520,
          'height':250
        };

        var geo_chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        geo_chart.draw(geo_data, geo_options);
         }


         ==========

         <body>
    <center>
   <div class="Header">
    <h1>Data Statistik Kepariwisataan Yogyakarta 2016</h1>
    </div>

    <!--Div that will hold the pie chart-->
    <table>
    <tr>
        <td colspan="5">
      <div id="chart_div" align='center'></div>
        </td>
    </tr>
    <tr>
      <td>
        <div id="chart_div2"></div>
      </td>
      <td>
        <div id="chart_div3"></div>
      </td>
      <td>
        <div id="chart_div4"></div>
      </td>
    </tr>
    <tr>
      <td colspan="5">
      <div id="chart_div5" align='center'></div>
      </td>
    </tr>
     
        </table>
        </center>
  </body>