<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chart extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//chart1
		$chartData=file_get_contents('assets/jumwisatawan.json');
		$chartData=json_decode($chartData);
		$res1=array();
		foreach($chartData as $row)
		{
			$dat1=[$row->bulan,(double)$row->val];
			array_push($res1, $dat1);
		}
		//echo json_encode($res);
		$data['PieChartTitle']='Jumlah Wisatawan yang berkunjung ke jogja berdasarkan bulan';
		$data['PieChartData']=json_encode($res1);


//chart2
		$chartData2=file_get_contents('assets/wisnushotel.json');
		$chartData2=json_decode($chartData2);
		$res2=array();
		foreach($chartData2 as $row)
		{
			$dat2=[$row->jenishotel,(double)$row->val];
			array_push($res2, $dat2);
		}
		//echo json_encode($res);
		$data['PieChartTitle2']='wisatawan nusantara pengguna hotel berbintang dan non bintang';
		$data['PieChartData2']=json_encode($res2);


//chart3
		$chartData3=file_get_contents('assets/jumlahwisatawan.json');
		$chartData3=json_decode($chartData3);
		$res3=array();
		foreach($chartData3 as $row)
		{
			$dat3=[$row->jenwisatawan,(double)$row->val];
			array_push($res3, $dat3);
		}
		//echo json_encode($res);
		$data['PieChartTitle3']='Wisatawan Nusantara dan Mancanegara';
		$data['PieChartData3']=json_encode($res3);

//chart4
		$chartData4=file_get_contents('assets/wismanhotel.json');
		$chartData4=json_decode($chartData4);
		$res4=array();
		foreach($chartData4 as $row)
		{
			$dat4=[$row->jenishotel,(double)$row->val];
			array_push($res4, $dat4);
		}
		//echo json_encode($res);
		$data['PieChartTitle4']='wisatawan mancanegara pengguna hotel berbintang dan non bintang';
		$data['PieChartData4']=json_encode($res4);

//chart5
		$chartData5=file_get_contents('assets/negaraasal.json');
		$chartData5=json_decode($chartData5);
		$res5=array();
		foreach($chartData5 as $row)
		{
			$dat5=[$row->NEGARA,(double)$row->TOWIS];
			array_push($res5, $dat5);
		}
		//echo json_encode($res);
		$data['PieChartTitle5']='wisatawan mancanegara pengguna hotel berbintang dan non bintang';
		$data['PieChartData5']=json_encode($res5);

		$this->load->view('grafik1',$data);
	}
	} 
	

